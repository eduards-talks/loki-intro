<!-- .slide: data-state="no-toc-progress" --> <!-- don't show toc progress bar on this slide -->

## Loki - Modern Log Aggregation that Scales
<!-- .element: class="no-toc-progress" --> <!-- slide not in toc progress bar -->


----  ----

## Where you are likely coming from

- Splunk
- Some form of elasticsearch
- Some cobbled together home grown solution
- ssh & grep

----

## Why log aggregation

- Correlation in one place
- Offloading of storage from servers
- Ephemeral Workloads

----

## Why Change?

- Ease of use and learning curve
- Scalability
- Metrics from logs
- Easier to do after the fact enrichment
- Performance

----  ----

## Enter Loki

- Uses Object Storage as persistence
- 'Grep' like experience
- Query time destructuring and enrichment
- Performance scales with demand not storage
  - Read/Write can be scaled separately
- Integrates with Grafana

----  ----

## Basic Concepts

----

## Labels & Streams

- Labels are assigned to log entries
- Every unique label set is a stream
- Familiar with timeseries data? Stream == Series
- A Stream selector is the first and only required query parameter
- e.g. `{source="app"}` would select all logs that are created by your own applications

----

## Labels & Streams ctd.

- A word of warning: "High Cardinality Kills"
  - Will cause huge indices and loads of tiny chunks
  - That is expensive and slow
  - Chunks are the actual data storage units and are stream and time based
  - Indices are used to locate the appropriate chunks for the searched time frame

----

## Wait? None of my data is indexed?

- Jup.
- All (non label) search is done as full text search on relatively small chunks
- This sound slow at first but it enables parallelization of search
- If you want to know more about why this is *not* a terrible idea talk to me later

Note:
Large indexes are complicated and expensive. Often a full-text index of your log data is the same size or bigger than the log data itself. To query your log data, you need this index loaded, and for performance, it should probably be in memory. This is difficult to scale, and as you ingest more logs, your index gets larger quickly.

Now let’s talk about Loki, where the index is typically an order of magnitude smaller than your ingested log volume. So if you are doing a good job of keeping your streams and stream churn to a minimum, the index grows very slowly compared to the ingested logs.

Loki will effectively keep your static costs as low as possible (index size and memory requirements as well as static log storage) and make the query performance something you can control at runtime with horizontal scaling.

This trade-off of smaller index and parallel brute force querying vs. a larger/faster full-text index is what allows Loki to save on costs versus other systems. The cost and complexity of operating a large index is high and is typically fixed – you pay for it 24 hours a day if you are querying it or not.

----

## Ingest

- JSON objects
- Format is simple. Basically

```js
{
  "source": "app",
  "system": "cool-service-1",
  "timestamp": "2020-01-01T18:00:01.750Z",
  "message": "What ever you send. Can also be string encoded json.",
}

```

----  ----

## Querying

- logcli
- Grafana
- RESTful API

Always using LogQl

----

## LogQL

First select a stream.

```
{source="app",service="proxy"}
|= "Chrome"
| json
| http_response_time > 600
```

- `=`: exactly equal
- `!=`: not equal
- `=~`: regex matches
- `!~`: regex does not match

----

## Line Matchers


- `|=`: Log line contains string
- `!=`: Log line does not contain string
- `|~`: Log line matches regular expression
- `!~`: Log line does not match regular expression

----

## Parsers

- json
- logfmt
- regexp (go2 fmt)

You can of course filter on the resulting labels as well.
_Hint:_ Use parsers towards the end of your query, i.e. as late as possible.


----

## Line Format Expressions

Rewrite logs as you go.

```
|line_format "👀 for {{.url_path}} with {{.http_response_status_code}} \
 by {{ if contains `Other` .user_agent_name }} \
 a 🤖 {{else}} a 👱‍♀️ {{end}}"
```

----

## Metric Aggregations

- rate
- count_over_time
- bytes_rate
- bytes_over_time
- absent_over_time

```
sum by (host) (
  rate({job="proxy"}
  |= "error" != "timeout"
  | json
  | duration > 10s [1m])
)
```

----

## Unwrap Expressions

Metrics basically work by counting lines (obviously not always true).
If you want to do metrics on a different field use unwrap expressions.

```
quantile_over_time(0.99,
  {service="cool-service-1",app="proxy"}
    | json
    | __error__ = ""
    | unwrap request_time [1m]) by (path)
```

----

## The Rest of It

Of course you also get

- Binary Operations
- Aggregation Operations
- Logical Operators
- Comparison Operators

----  ----

## Trail

This is the part where stuff gets interesting because things can go wrong ;)
